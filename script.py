import os
import re

src = "."
jsFiles = []
scssFiles = []
matchedList = [1000]


def main():
    doAllFolders(src)
    print(jsFiles)
    print (scssFiles)
    createJsFiles(src)


def createJsFiles(src):
    rawCssLines = [1, 2, 3]
    editedCSS = [1, 2, 3]
    for root, dirList, files in os.walk(src):
        for file in files:
            if (file.endswith('.js')):
                for f in files:
                    if (f.endswith('.scss')):
                        jsn = str(file).strip('.js')
                        csn = str(f).strip('.scss')
                        if(jsn == csn):
                            rawCssLines[:] = []
                            editedCSS[:] = []
                            rawCssLines = cssToList(os.path.join(root, f))
                            editedCSS = cssListToEmotion(rawCssLines)
                            with open(os.path.join(root, file), 'r+') as bish:
                                OG = bish.read()
                                bish.seek(0, 0)
                                for line in editedCSS:
                                    bish.write('%s' % line)
                                bish.write(OG)
                                OG = ""


def doAllFolders(src):
    for root, dirList, files in os.walk(src):
        for name in files:
            if (name.endswith('.js')):
                jsFiles.append(os.path.join(root, name))
            if (name.endswith('.scss')):
                scssFiles.append(os.path.join(root, name))


def cssToList(cssF):
    with open(cssF, 'r') as f:
        cssLines = []
        cssLines[:] = []
        for line in f:
            cssLines.append(line)
    return cssLines


# match js and css files
def addAllFiles(source):
    for dirName, dirList, files in os.walk(source):
        for file in files:
            # f = open(file, 'r')
            # if (f.mode == 'r'):
            if (file.endswith(".js")):
                jsFiles.append(file)
            elif(file.endswith(".scss")):
                scssFiles.append(file)


# edit css to be emotion objects
def cssListToEmotion(lines):
    editedList = []
    for line in lines:
        lenL = len(line)

        # working only on declaration lines
        if (line[0] == '.'):
            line = re.sub('[{ .\n]', '', line)
            line = "const " + line + " = css({\n"

        # working on css command lines only
        elif(line[lenL-2] == ';'):

            # replace '-' in lines unless its a -ve symbol
            if '-' in line:
                ddx = line.find('-')+1
                if(line[ddx] not in ['0', '1', '2', '3', '4',
                                     '5', '6', '7', '8', '9']):
                    line = ''.join([line[:ddx], line[ddx].upper(),
                                    line[ddx + 1:]])
                    line = re.sub('[-]', '', line)

            # remove px from end of lines or put quotes around content
            lenL = len(line)
            if ('px;' in line):
                line = line.strip('px;\n') + ',' + '\n'
            elif((':' in line) & (';' in line)):
                col = line.find(':')
                sem = line.find(';')
                line = line[:col+2] + "'" + line[col+2:sem] + "'" + line[sem:]

            # replace semicolons with commas
            line = re.sub('[;]', ',', line)
        else:
            if ('@media' in line):
                curl = line.find('{')
                at = line.find('@')
                line = line[:at] + "'" + line[at:curl] + "':{\n"
        # print (line)
        editedList.append(line)
    return (editedList)


if __name__ == '__main__':
    main()
